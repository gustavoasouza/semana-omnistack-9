const express   = require('express')
const mongoose  = require('mongoose')
const cors      = require('cors')
const path      = require('path')

const routes    = require('./routes')
const app       = express()

mongoose.connect('mongodb+srv://omnistack:omnistack@cluster0-gx4nl.mongodb.net/semana09?retryWrites=true&w=majority', {
    useNewUrlParser: true,
    useUnifiedTopology: true
}).then( () => {
    console.log('Banco conectado com sucesso')
}).catch ( (erro) => {
    console.log(`Erro ao se conectar: ${erro}`)
})

// req.query serve para acessar os query params (filtros)
// req.params serve para acessar os route params (edição e exclusão)
// req.body serve para acessar o corpo da requisição (edição e criação)

//Por padrão express não reconhece JSON
//Aqui estamos declarando para que o express use o JSON
app.use(cors())
app.use(express.json())
app.use('/files', express.static(path.resolve(__dirname, '..', 'uplouds')))
app.use(routes)

app.listen(3238)