const User = require('../models/User')

module.exports = {
    //Criando uma sessão
    async store(req, res) {

        //Buscando email de dentro de req.body
        const { email } = req.body

        let user = await User.findOne({ email })

        if (!user) {
            //Só deixa prosseguir pra próxima linha quando terminar o adastro do banco
            user = await User.create({ email })
        }

        return res.json(user)
    }
}