# Backend

## Arquivos:
- *package-lock.json* é um arquivo que contém o cache de nossas dependências.

## Pastas:
- *src* é onde está todo o código da aplicação. (Boa prática separar ele dos arquivos de configuração).
- *node_modules* é onde ficam armazenados os códigos das dependências.

## Dependências:
- Express: Micro framework que auxilia na definição de rotas.

### Flags:
- *-D* Define uma dependência como sendo "devDependencies", ou seja, nao será usada em produção.

## Scripts:
Para executar um script, basta usar o comando ``` npm run <nome do script> ```.

### Criando um script:
```
  "scripts": {
    "dev": "nodemon src/server.js"
  }
```

## Rotas:
- GET: Pegar dados.
- POST: Enviar dados. (Navegadores não conseguem ler rotas do tipo POST).
- PUT: Alterar dados.
- DELETE: Deletar dados.

Nesse caso é recomendado usar um software para testes de rotas (APIs).