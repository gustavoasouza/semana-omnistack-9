import axios from 'axios';

const api = axios.create({
    baseURL: 'http://localhost:3238'
})

export default api;