import React, { useState } from 'react';
import api from '../../services/api';

export default function Login({ history }) {
    //useState retornando um vetor
  const [email, setEmail] = useState('')

  async function handleSubmit(event) {
    //Evitando que o usuário seja redirecionado para outra tela. (Comportamento padrão)
    event.preventDefault()

    //Quando devolver uma resposta, armazenar a mesma na variavel response
    const response = await api.post('/sessions', { email });

    const { _id } = response.data;

    //Salvando no banco de dados local do navegador
    localStorage.setItem('user', _id);

    history.push('/dashboard')
  }

    return (
    // Isolando o componente com <> fragment
    <>

    <p>
      Ofereça <strong>spots</strong> para programadores e encontre <strong>talentos</strong> para sua empresa.
    </p>

            <form onSubmit={handleSubmit}>
                <label htmlFor="email">E-MAIL *</label>
                <input
                    type="text"
                    id="email"
                    placeholder="Seu melhor e-mail"
                    value={email}
                    onChange={event => setEmail(event.target.value)}
                />

                <button
                    type="submit"
                    className="btn" >Entrar</button>
            </form>

        </>
    )
}