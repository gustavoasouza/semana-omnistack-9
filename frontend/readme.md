# Frontend
Geralmente quando se utiliza a arquitetura MVC, view (frontend) é retornado diretamente do backend.

## Como funciona a separação?
Uma backend isolado fornece os dados em JSON para um frontend consumir e montar a interface.

## Vantagens de separar o backend do frontend:
- O mesmo backend fornecer dados para inúmeros frontend.
- Criar um frontend web e um frontend mobile a partir do mesmo backend.

## Observações ao usar o React
React é uma bilbioteca que roda no browser, por tanto, existem algumas funcionalidade do react que alguns browsers não reconhecem. Para sanar esse problema o React usa uma biblioteca por baixo dos panos que se chama babel.

### Babel:
Transpila código moderno de JavaScript para uma versão em que qualquer navegador pode entender.

Para não precisar ficar instalando o babel e as demais dependências toda vez que for criar um projeto, o React já tem pronto um pacote ``` create-react-app ``` que cria toda a estrutura necessária do projeto.

### Criando um novo projeto React:
- ``` npx create-react-app <nome do projeto> ``` Cria a estrutura do projeto React.
- ``` npm run start ``` Starta o projeto React.

### Como o React funciona?
React e outras libs são baseados em um conceito chamado de componentização.

A componentização nos permite reutilizar código, ou seja, nós criamos componentes, tudo que está na tela são componentes.

Componentes podem ser vistos como um conjunto isolado de código HTML, CSS e JS que ele por sí só, tem um funcionamento.

#### Quando criar um componente?
Quando tiver um "pedaço da sua tela" que possuí um funcionamento isolado ou algo que você consegue reutilizar.

Ex: Um formulário de "Fale conosco".

### Formato de um componente:
Um componente nada mais é do que uma função que retorna um HTML.
Essa sintaxe é chamada de *JSX*.

### Consumindo APIs:
Realizar o consumo de APIs direito do frontend.

#### Axios:
É uma biblioteca que permite lidar com chamadas API.

- ``` npm install axios --save ``` Instala o axios.

### Estados no React
Estados são informações que nós armazenamos dentro de um componente.

Toda informação que você vai manipular em um componente é armazenada dentro de um estado.

### Rotas
Cada página é criada em uma pasta separada, onde cada pasta conterá o arquivo de estilo.